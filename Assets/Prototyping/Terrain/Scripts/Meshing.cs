﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Prototyping
{
	// Texture Atlas Information
	[System.Serializable]
	public struct AtlasInfo
	{
		// atlas index specified from top left of image
		/// <summary>
		/// Width of atlas in pixels
		/// </summary>
		public int width;
		/// <summary>
		/// Height of atlas in pixels
		/// </summary>
		public int height;
		/// <summary>
		/// Size of tiles, in pixels!
		/// </summary>
		public int tileSize;
		/// <summary>
		/// Padding around tiles, in pixels!
		/// </summary>
		public int padding;

		public IntVector2 GetPixelOffset(IntVector2 tilePosition)
		{
			return new IntVector2(tilePosition.x * (tileSize + 2 * padding), height - ((tilePosition.y + 1) * tileSize + (tilePosition.y * 2 * padding)));
		}

		public int GetAtlasIndex(IntVector2 atlasPosition)
		{
			return atlasPosition.x + atlasPosition.y * this.width;
		}
	}

	[System.Serializable]
	public struct AtlasPosition
	{
		public IntVector2 side;
		public IntVector2 top;
		public IntVector2 bottom;
	}

	public struct TransparentBlock
	{
		public int chunkIndex;
		public int blockIndex;
		public MeshData meshData;
	}

	public struct BlockData
	{
		public int blockId;
		public int atlasIndex;
		public MeshData meshData;
	}

	public static class Meshing
	{

		#region Greedy Mesh (WIP)

		// Per Block Type
		//  Per 'Direction' (or plane w/ +ve and -ve directions)
		//   Start 'top-left' go 'right' and then 'up' alternating and make quad until can't go further, reset to top left most remaining.
		//   MeshData per blockId if not transparent
		//   MeshData per face per blockId if liquid || transparent
		//   Maybe: MeshData per block if transparent and not liquid (will find out)

		// Note complicated by orentation

		// Should probably update the shader to not require uv coordinates

		private enum GreeyPlaneTransform {
			None,
			Pitch,
			Yaw,
		}; 
		// ^^ There should be a mathematical way of doing this presumably?
		// Need to reread my matrix work

		// Question... wouldn't it be easier to create the naive mesh, but separate by atlas index (and block id)
		// then just run a generic mesh optimisation routine?
		public static List<BlockData> CreateGreedyMeshData(World world, Chunk chunk, BlockConfig config)
		{
			var data = new List<BlockData>();

			var blockIds = chunk.GetBlockIds();

			foreach(var id in blockIds)
			{
				// TODO: Group by orientations, for now all are up!
				var blockData = new BlockData { 
					blockId = id,
					meshData = new MeshData(),
				};

				// oh we need a dictionary by atlas index, then need to determine atlas index
				// based on oreientation / and direction... jesus I just want to test the freaking quad stuff

				// okay so to test quading, just use the side... atlas index
				var sideAtlasIndices = config.blocks[id].atlasIndices.side;
				blockData.atlasIndex = config.atlasInfo.GetAtlasIndex(sideAtlasIndices);

				for (int planeDepthIndex = 0; planeDepthIndex < chunk.size; planeDepthIndex++)
				{
					
					// Start with X-Y
					// Direction is Forward / Back
					// Need to either extract the methods to delegates or provide
					// a cyclic index to allow us to change the world querying
					var positiveDirection = new IntVector3(Vector3.forward);
					var negativeDirection = new IntVector3(Vector3.back);
					var planeMeshData = new MeshData();
				
					// if transparent add planeMeshData to data list
					if (config.blocks[id].isTransparent)
					{
						// This might only be valid if on isLiquid
						BuildPlaneMeshData(world, chunk, config, id, planeDepthIndex, positiveDirection, planeMeshData);
						data.Add(new BlockData { 
							blockId = blockData.blockId,
							atlasIndex = blockData.atlasIndex,
							meshData = planeMeshData,
						});

						planeMeshData = new MeshData();
						BuildPlaneMeshData(world, chunk, config, id, planeDepthIndex, negativeDirection, planeMeshData);
						data.Add(new BlockData {
							blockId = blockData.blockId,
							atlasIndex = blockData.atlasIndex,
							meshData = planeMeshData,
						});
					}
					else
					{
						// if not combine planeMeshData w/ blockMeshData
						BuildPlaneMeshData(world, chunk, config, id, planeDepthIndex, positiveDirection, planeMeshData);
						BuildPlaneMeshData(world, chunk, config, id, planeDepthIndex, negativeDirection, planeMeshData);

						blockData.meshData.Combine(planeMeshData);
					}
				}

				// if not transparent add blockMeshData to data list
				if (!config.blocks[id].isTransparent)
				{
					data.Add(blockData);
				}
			}
			return data;
		}

		private static IntVector3 BuildPlaneMeshData(
			World world,
			Chunk chunk,
			BlockConfig config,
			int id,
			int planeDepthIndex,
			IntVector3 direction,
			MeshData planeMeshData)
		{
			var indicesConsidered = new Dictionary<int, HashSet<int>>(); // x -> y

			for (int planeHorizontalIndex = 0; planeHorizontalIndex < chunk.size; planeHorizontalIndex++)
			{
				// Can build positive and negative direction planes at the same time
				for (int planeVerticalIndex = 0; planeVerticalIndex < chunk.size; planeVerticalIndex++)
				{
					// If liquid need to consider empty blocks with edge to liquid too (internal faces)

					// Worth noting we need to do it by orientation too (well they need to have the same atlas index anyway)
					// Although there is a potential optimisation there... (presuming that all other properties are the same
					// you could combine different block types where orientation means they have the same atlas indices
					// e.g. soil and grass bottom)
					if (indicesConsidered.ContainsKey(planeHorizontalIndex)
						&& indicesConsidered[planeHorizontalIndex].Contains(planeVerticalIndex))
					{
						continue;
					}

					if (chunk.GetBlock(planeHorizontalIndex, planeVerticalIndex, planeDepthIndex) == id
						&& IsVisibleEdge(world, chunk, config, chunk.GetBlock(planeHorizontalIndex, planeVerticalIndex, planeDepthIndex), planeHorizontalIndex + direction.x, planeVerticalIndex + direction.y, planeDepthIndex + direction.z))
					{
						int quadHorizontalIndex = planeHorizontalIndex + 1;
						// 'Bottom Left' is x,y
						// 'Top Left' is x+1,y
						// 'Bottom Right' is x,y
						// 'Top Right' is x+1,y+1

						// although it's complicated by the fact the niave mesh currently places voxels centred on their coordinates
						// might need to change that, depends on the shader we write

						while (chunk.GetBlock(quadHorizontalIndex, planeVerticalIndex, planeDepthIndex) == id
							&& IsVisibleEdge(world, chunk, config, chunk.GetBlock(quadHorizontalIndex, planeVerticalIndex, planeDepthIndex), quadHorizontalIndex + direction.x, planeVerticalIndex + direction.y, planeDepthIndex + direction.z))
						{
							// Increase Bottom Right and Top Right
							quadHorizontalIndex++;
						}

						// Check if can go 'up'
						bool canExtend = true;
						int quadVerticalIndex = planeVerticalIndex + 1;
						while (canExtend && quadVerticalIndex < chunk.size)
						{
							for (int i = planeHorizontalIndex; i < quadHorizontalIndex; i++)
							{
								if (chunk.GetBlock(i, quadVerticalIndex, planeDepthIndex) != id
									|| !IsVisibleEdge(world, chunk, config, chunk.GetBlock(i, quadVerticalIndex, planeDepthIndex), quadHorizontalIndex + direction.x, quadVerticalIndex + direction.y, planeDepthIndex + direction.z))
								{
									canExtend = false;
									break;
								}
							}

							if (canExtend)
							{
								// Increase Top Left and Top Right 
								quadVerticalIndex++;
							}
						}

						// Now have our size for this quad add it to mesh data
						InsertMeshData(planeMeshData, chunk, planeHorizontalIndex, quadHorizontalIndex, planeVerticalIndex, quadVerticalIndex, planeDepthIndex, direction, true);

						// Have considered planeHorizontalIndex -> quadHorizontalIndex - 1 and planeVerticalIndex -> quadVerticalIndex-1
						// do not need to investigate these again
						for (int i = planeHorizontalIndex; i < quadHorizontalIndex; i++)
						{
							if (!indicesConsidered.ContainsKey(i))
							{
								indicesConsidered.Add(i, new HashSet<int>());
							}
							for (int j = planeVerticalIndex; j < quadVerticalIndex; j++)
							{
								indicesConsidered[i].Add(j);
							}
						}
						planeHorizontalIndex = quadHorizontalIndex;
					}
				}
			}
			return direction;
		}

		private static void InsertMeshData(
			MeshData planeMeshData,
			Chunk chunk,
			int planeHorizontalIndex,
			int quadHorizontalIndex,
			int planeVerticalIndex,
			int quadVerticalIndex,
			int planeDepthIndex,
			IntVector3 direction,
			bool directionIsPositive)
		{
			// Note: horizontal !=> x, vertical !=> y & depth != z;
			// directionIsPositive doesn't just set the 'z' position but also the cyclic order of vertices

			// The question is which way horizontal, vertical and depth point in global space,
			// this is determined by the chunk indexing convention.
			// There are only three options as the inverts are in the 'direction' data

			float iMin, iMax, jMin, jMax, k;
			iMin = (planeHorizontalIndex - Mathf.Floor(chunk.size / 2f)) - 0.5f;
			iMax = (quadHorizontalIndex - Mathf.Floor(chunk.size / 2f)) - 0.5f;
			jMin = (planeVerticalIndex - Mathf.Floor(chunk.size / 2f)) - 0.5f;
			jMax = (quadVerticalIndex - Mathf.Floor(chunk.size / 2f)) - 0.5f;
			k = (planeDepthIndex - Mathf.Floor(chunk.size / 2f)) - 0.5f * (directionIsPositive ? 1 : -1);
			
			var vertices = new List<Vector3> { 
				new Vector3(iMin, jMin, k),
				new Vector3(iMin, jMax, k),
				new Vector3(iMax, jMin, k),
				new Vector3(iMax, jMax, k),
			};
			var normals = new List<Vector3> { 
				direction.ToVector3(),
				direction.ToVector3(),
				direction.ToVector3(),
				direction.ToVector3(),
			};

			var uvs = new List<Vector2>();
			for (int i = 0; i < 4; i++)
			{
				uvs.Add((Vector2)vertices[i]);
			}

			if (direction.z != 0)
			{
				// Option 1 - No transform
				// z = global.forward (k)
				// y = global.up (j)
				// x = global.right (i)

				// No action
				
			}
			else if (direction.y != 0)
			{
				// Option 2 - Pitch Transform
				// z = global.up (j)
				// y = global.forward (k)
				// x = global.right (i)

				// Swap y and z
				for(int i = 0; i < 4; i++)
				{
					vertices[i] = new Vector3(vertices[i].x, vertices[i].y, vertices[i].z);
				}
			}
			else if (direction.z != 0)
			{
				// Option 3 - Yaw Transform
				// z = global.right (i)
				// y = globla.up (j)
				// x = global.forward (k)
				
				// Swap x and z
				for(int i = 0; i < 4; i++)
				{
					vertices[i] = new Vector3(vertices[i].z, vertices[i].y, vertices[i].x);
				}
			}
			else
			{
				throw new UnityException("Greedy meshing internal error; a non-zero direction vector is required");
			}

			List<int> trianges;
			if (directionIsPositive)
			{
				trianges = new List<int> { 
					0, 1, 2,
					1, 3, 2,
				};
			}
			else
			{
				trianges = new List<int> { 
					0, 2, 1,
					1, 2, 3,
				};
			}

			var quadMeshData = new MeshData {
				vertices = vertices,
				normals = normals,
				uvs = uvs,
				triangles = trianges,
			};

			planeMeshData.Combine(quadMeshData);
		}

		#endregion

		public static List<BlockData> CreateCulledMeshData(World world, Chunk chunk, BlockConfig config)
		{
			// Okay lets do this without rotation, first
			// that still implies need to separate by top, side, bottom

			var meshDataByBlock = new Dictionary<int, List<MeshData>>();
			for (int i = 0; i < chunk.size; i++)
			{
				float x = (i - Mathf.Floor(chunk.size / 2f));
				for (int j = 0; j < chunk.size; j++)
				{
					float y = (j - Mathf.Floor(chunk.size / 2f));
					for (int k = 0; k < chunk.size; k++)
					{
						float z = (k - Mathf.Floor(chunk.size / 2f));
						var block = config.blocks[chunk.GetBlock(i, j, k)];

						if (!meshDataByBlock.ContainsKey(block.id))
						{
							meshDataByBlock[block.id] = new List<MeshData>();
						}
						var list = meshDataByBlock[block.id];

						if (!block.isTransparent)
						{
							if (list.Count == 0)
							{
								list.Add(new MeshData());
							}
							AddBlockToMesh(list[0], world, chunk, config, i, j, k, x, y, z);
							// TODO: Separate by sides
						}
						else if (block.id != 0)
						{
							// TODO: should spawn edges to liquid either in air blocks or double sided in liquid block
							var transparentMesh = new MeshData();
							AddBlockToMesh(transparentMesh, world, chunk, config, i, j, k, x, y, z);
							meshDataByBlock[block.id].Add(transparentMesh);
						}
					}
				}
			}

			var result = new List<BlockData>();

			foreach(var blockId in meshDataByBlock.Keys)
			{
				foreach(var meshData in meshDataByBlock[blockId])
				{
					var atlasIndices = config.blocks[blockId].atlasIndices.top;
					// TODO ^^ : Look that up properly
					result.Add(new BlockData { 
						blockId = blockId,
						atlasIndex = config.atlasInfo.GetAtlasIndex(atlasIndices),
						meshData = meshData,
					});
				}
			}

			return result;
		}

		// Should refactor this, to a type taking world and config in constructor and CreateMeshData taking just chunk and outputing 
		// List<BlockData> then change the instantiation code accordingly, that way greedy mesh will be easier to implement
		// note that naive culled mesh will just need to put -1 in the shared solid blockid... but that's fine
		public static MeshData CreateCulledMeshData(World world, Chunk chunk, BlockConfig config, List<TransparentBlock> transparentBlocks)
		{
			// Note Unity has a maximum number of vertices
			// Chunk size of 16 should work... 16^3 x 24 is more than 6500
			// but you can't have all squares occupied...
			var meshData = new MeshData();
			for (int i = 0; i < chunk.size; i++)
			{
				float x = (i - Mathf.Floor(chunk.size / 2f));
				for (int j = 0; j < chunk.size; j++)
				{
					float y = (j - Mathf.Floor(chunk.size / 2f));
					for (int k = 0; k < chunk.size; k++)
					{
						float z = (k - Mathf.Floor(chunk.size / 2f));
						var block = config.blocks[chunk.GetBlock(i,j,k)];
						if (!block.isTransparent)
						{
							AddBlockToMesh(meshData, world, chunk, config, i, j, k, x, y, z);
						}
						else if (block.id != 0)
						{
							// TODO: should spawn edges to liquid either in air blocks or double sided in liquid block
							var transparentMesh = new MeshData();
							AddBlockToMesh(transparentMesh, world, chunk, config, i, j, k, x, y, z);
							if (transparentMesh.triangles.Count > 0)
							{
								transparentBlocks.Add(new TransparentBlock {
									blockIndex = block.id,
									meshData = transparentMesh,
								});
							}
						}
					}
				}
			}
			return meshData;
		}

		private static void AddBlockToMesh(MeshData mesh, World world, Chunk chunk, BlockConfig config, int i, int j, int k, float x, float y, float z)
		{
			var block = chunk.GetBlock(i, j, k);
			var orientation = chunk.GetBlockOrientation(i, j, k);
			if (block == 0) { return; }		// By convention block == 0 => Empty
			var indices = config.blocks[block].atlasIndices;
			int rotations = 0;
			// For Each Direction : Is Visible Edge? Add quad to mesh!
			// Front
			if (IsVisibleEdge(world, chunk, config, block, i, j, k + 1))
			{
				var atlasPosition = CalculateAtlasPositionFromOrientation(indices, Chunk.Orientation.Foward, orientation, out rotations);
				AddQuadToMesh(mesh, config.atlasInfo, atlasPosition, CubeMeshData.Front, x, y, z, rotations);
			}
			// Back
			if (IsVisibleEdge(world, chunk, config, block, i, j, k - 1))
			{
				var atlasPosition = CalculateAtlasPositionFromOrientation(indices, Chunk.Orientation.Back, orientation, out rotations);
				AddQuadToMesh(mesh, config.atlasInfo, atlasPosition, CubeMeshData.Back, x, y, z, rotations);
			}
			// Top
			if (IsVisibleEdge(world, chunk, config, block, i, j + 1, k))
			{
				var atlasPosition = CalculateAtlasPositionFromOrientation(indices, Chunk.Orientation.Up, orientation, out rotations);
				AddQuadToMesh(mesh, config.atlasInfo, atlasPosition, CubeMeshData.Top, x, y, z, rotations);
			}
			// Bottom
			if (IsVisibleEdge(world, chunk, config, block, i, j - 1, k))
			{
				var atlasPosition = CalculateAtlasPositionFromOrientation(indices, Chunk.Orientation.Down, orientation, out rotations);
				AddQuadToMesh(mesh, config.atlasInfo, atlasPosition, CubeMeshData.Bottom, x, y, z, rotations);
			}
			// Right
			if (IsVisibleEdge(world, chunk, config, block, i + 1, j, k))
			{
				var atlasPosition = CalculateAtlasPositionFromOrientation(indices, Chunk.Orientation.Right, orientation, out rotations);
				AddQuadToMesh(mesh, config.atlasInfo, atlasPosition, CubeMeshData.Right, x, y, z, rotations);
			}
			// Left
			if (IsVisibleEdge(world, chunk, config, block, i - 1, j, k))
			{
				var atlasPosition = CalculateAtlasPositionFromOrientation(indices, Chunk.Orientation.Left, orientation, out rotations);
				AddQuadToMesh(mesh, config.atlasInfo, atlasPosition, CubeMeshData.Left, x, y, z, rotations);
			}
		}

		private static bool IsVisibleEdge(World world, Chunk chunk, BlockConfig config, int blockId, int i, int j, int k)
		{
			Block currentBlock = config.blocks[blockId];
			Block adjacentBlock = config.blocks[world.GetBlock(chunk, i, j, k)];
			if (!currentBlock.isLiquid)
			{
				return adjacentBlock.isTransparent;
			}
			else
			{
				return adjacentBlock.isTransparent && adjacentBlock.id != blockId;
			}
		}

		private static IntVector2 CalculateAtlasPositionFromOrientation(
			AtlasPosition indices,
			Chunk.Orientation worldDirection,
			Chunk.Orientation blockOrientation,
			out int uvRotations)
		{
			uvRotations = 0;

			// There's probably a clever way to do this with vectors
			// but I'm watching Top Gear and easier just to type this out
			// oh god, and the rotation is so dependent on the mesh data used it's not even funny
			if (blockOrientation == Chunk.Orientation.Up)
			{
				switch(worldDirection)
				{
					case Chunk.Orientation.Up:
						return indices.top;
					case Chunk.Orientation.Down:
						return indices.bottom;
					default:
						return indices.side;
				}
			}
			if (blockOrientation == Chunk.Orientation.Down)
			{
				switch(worldDirection)
				{
					case Chunk.Orientation.Up:
						return indices.bottom;
					case Chunk.Orientation.Down:
						return indices.top;
					default:
						uvRotations = 2;
						return indices.side;
				}
			}
			if (blockOrientation == Chunk.Orientation.Foward)
			{
				switch(worldDirection)
				{
					case Chunk.Orientation.Foward:
						return indices.top;
					case Chunk.Orientation.Back:
						return indices.bottom;
					default:
						if (worldDirection == Chunk.Orientation.Right)
						{
							uvRotations = 1;
						}
						else if (worldDirection == Chunk.Orientation.Up || worldDirection == Chunk.Orientation.Down)
						{
							uvRotations = 2;
						}
						else
						{
							uvRotations = 3;
						}
						return indices.side;
				}
			}
			if (blockOrientation == Chunk.Orientation.Back)
			{
				switch(worldDirection)
				{
					case Chunk.Orientation.Back:
						return indices.top;
					case Chunk.Orientation.Foward:
						return indices.bottom;
					default:
						if (worldDirection == Chunk.Orientation.Left)
						{
							uvRotations = 1;
						}
						else if (worldDirection == Chunk.Orientation.Right)
						{
							uvRotations = 3;
						}
						return indices.side;
				}
			}
			if (blockOrientation == Chunk.Orientation.Left)
			{
				switch(worldDirection)
				{
					case Chunk.Orientation.Left:
						return indices.top;
					case Chunk.Orientation.Right:
						return indices.bottom;
					default:
						if (worldDirection == Chunk.Orientation.Back || worldDirection == Chunk.Orientation.Down)
						{
							uvRotations = 3;
						}
						else
						{
							uvRotations = 1;
						}
						return indices.side;
				}
			}
			if (blockOrientation == Chunk.Orientation.Right)
			{
				switch(worldDirection)
				{
					case Chunk.Orientation.Right:
						return indices.top;
					case Chunk.Orientation.Left:
						return indices.bottom;
					default:
						if (worldDirection == Chunk.Orientation.Back || worldDirection == Chunk.Orientation.Down)
						{
							uvRotations = 1;
						}
						else
						{
							uvRotations = 3;
						}
						return indices.side;
				}
			}
			throw new UnityException("Unrecognised Block Orientation " + blockOrientation.ToString());
		}

		private static void AddQuadToMesh(MeshData meshData, AtlasInfo atlasInfo, IntVector2 atlasPosition, MeshData face, float x, float y, float z, int cycleUvs = 0)
		{
			meshData.Combine(face, new Vector3(x, y, z), 0.5f);
			int uvIndexOffset = meshData.uvs.Count - 4;
			var atlasOffset = atlasInfo.GetPixelOffset(atlasPosition);
			meshData.AdjustQuadTextureCoordinates(uvIndexOffset, atlasInfo.tileSize, atlasInfo.width, atlasInfo.height, atlasOffset.ToFloatArray(), cycleUvs);
		}
	}
}
