﻿using UnityEngine;
using System.Collections;

namespace Prototyping
{
	public class CavernTerrainRules : TerrainRules
	{
		// Debug Variables
		int count;
		float average;

		// Perlin Height Map
		//[SerializeField]
		//float[] octaveWeightings = new float[] { 1f, 0.5f, 0.25f };
		//[SerializeField]
		//float baseWavelength = 64f;

		[SerializeField]
		float pillarSpacing = 32;
		[SerializeField]
		float xHalfPoint = 4;		// Apparently "(value of x you want y = 0.5 at)" not sure what this means, maybe y = distance field output
		[SerializeField]
		float yDistanceToFullAttentuation = 16;
		[SerializeField]
		float yBuffer = 0;
		[SerializeField]
		float yRoof = 48;

		public override float BaseFunction(float x, float y, float z)
		{
			float tx, ty, tz;
			tx = Mathf.Abs(x) % pillarSpacing;
			if (tx > 0.5f * pillarSpacing)
			{
				tx = pillarSpacing - tx;
			}
			tz = Mathf.Abs(z) % pillarSpacing;
			if (tz > 0.5f * pillarSpacing)
			{
				tz = pillarSpacing - tz;
			}

			float xzDistanceField = Mathf.Sqrt(tx * tx + tz * tz);
			// 0 to inf, with 0 at pillar points
			xzDistanceField = 1 - xHalfPoint / (xzDistanceField + 1);
			// remap to 0 -> 1

			ty = (yRoof - yBuffer - y);
			float yAttenuation = Mathf.SmoothStep(0, 1, ty / yDistanceToFullAttentuation);

			return 2.5f * (1 - (yAttenuation * xzDistanceField)); // Multiplied by a constant factor to offset being multiplied by noise functions (which will reduce the value)
		}


		//public override BaseFunction(float x, float y, float z)
		//{
		//	var p = PerlinHeightmap(x, z);
		//	// TODO: Clamp to Inverted Perlin Heightmap, greater than threshold return 1

		//	if (p > 0.65f) // Higher number == less / smaller pillars
		//	{
		//		return 1;
		//	}

		//	if (y > 40)
		//	{
		//		float exageratedHeightMap = (2 * p) * (2 * p) * (2 * p);
		//		// ^^ 2p * 2p should mean everything above 0.5 is bigger porportionally?

		//		return 6 * (exageratedHeightMap - (54 - y) / 4f); // Wrong way up...
		//		// return 6 * (exageratedHeightMap + (40 - y) / 4f); // Wrong way up...
		//	}

		//	return 0;
		//}

		//private float PerlinHeightmap(float x, float y)
		//{
		//	float value = 0f, totalWeight = 0f;

		//	for (int o = 0, l = octaveWeightings.Length; o < l; o++)
		//	{
		//		var waveLength = Mathf.Pow(2, o);
		//		totalWeight += octaveWeightings[o];
		//		value += octaveWeightings[o] * Mathf.PerlinNoise(waveLength * x / baseWavelength, waveLength * y / baseWavelength);
		//	}

		//	value /= totalWeight;

		//	return value;
		//}

		public override int MapNoiseToBlock(float noiseValue)
		{
			average = average * count;
			count++;
			average += noiseValue;
			average /= count;
			return noiseValue > 0.1f ? 1 : 0;
		}

		public override int TransformBlock(
			World worldInfo,
			Chunk currentChunk,
			float worldY,
			int x,
			int y,
			int z,
			int block)
		{
			return block;
		}
	}
}

