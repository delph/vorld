﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Prototyping.UI
{
	public class GenerationPanel : MonoBehaviour
	{
		[SerializeField]
		InputField seedValue;
		[SerializeField]
		InputField horizontalExtents;
		[SerializeField]
		InputField verticalExtents;
		[SerializeField]
		InputField baseWavelength;
		[SerializeField]
		InputField octaveCount;
		[SerializeField]
		RectTransform weightingsContainer;
		[SerializeField]
		GameObject octaveWeightingTemplate;
		[SerializeField]
		Button startGenerationButton;

		GameObject[] octaveWeightingGameObjects = new GameObject[0];
		InputField[] octaveWeightingInputs = new InputField[0];

		// TODO: Field validation to only allow numbers

		public void Init(VoxelTerrain voxelTerrain)
		{
			var initialValues = voxelTerrain.GetCurrentGenerationParameters();
			startGenerationButton.onClick.AddListener(() => {
				voxelTerrain.StartGenerationRoutine(GetGenerationParameters());
				gameObject.SetActive(false);
			});
			
			seedValue.text = initialValues.Seed.ToString();
			seedValue.onEndEdit.AddListener((value) => {
				int result;
				if (!int.TryParse(value, out result) || result < 0)
				{
					seedValue.text = "0";
				}
			});

			horizontalExtents.text = initialValues.HorizontalExtents.ToString();
			horizontalExtents.onEndEdit.AddListener((value) =>
			{
				int result = 0;
				if (!int.TryParse(value, out result))
				{
					horizontalExtents.text = "32";
				}
				else if (result < 1 || result > 128)
				{
					horizontalExtents.text = Mathf.Clamp(result, 1, 128).ToString();
				}
			});

			verticalExtents.text = initialValues.VerticalExtents.ToString();
			verticalExtents.onEndEdit.AddListener((value) => {
				int result = 0;
				if (!int.TryParse(value, out result))
				{
					verticalExtents.text = "4";
				}
				else if(result < 1 || result > 8)
				{
					verticalExtents.text = Mathf.Clamp(result, 1, 8).ToString();
				}
			});

			baseWavelength.text = initialValues.BaseWavelength.ToString();
			baseWavelength.onEndEdit.AddListener((value) => {
				int result = 0;
				if (!int.TryParse(value, out result) || result < 1 || result > 256)
				{
					baseWavelength.text = Mathf.Clamp(result, 1, 256).ToString();
				}
			});

			octaveCount.onEndEdit.AddListener((value) =>
			{
				int result;
				if (int.TryParse(value, out result) && result > 0)
				{
					SetOctaveWeightingCount(result);
				}
				else
				{
					octaveCount.text = "1";
					SetOctaveWeightingCount(1);
				}
			});

			SetOctaveWeightingCount(initialValues.OctaveWeightings.Length);
			for(int i = 0, l = initialValues.OctaveWeightings.Length; i < l; i++)
			{
				octaveWeightingInputs[i].text = initialValues.OctaveWeightings[i].ToString();
			}
		}

		public void SetOctaveWeightingCount(int count)
		{
			octaveCount.text = count.ToString();
			var newGos = new GameObject[count];
			var newInputs = new InputField[count];
			for(int i = 0, l = octaveWeightingInputs.Length; i < l; i++)
			{
				if (i < count)
				{
					newGos[i] = octaveWeightingGameObjects[i];
					newInputs[i] = octaveWeightingInputs[i];
				}
				else
				{
					Object.Destroy(octaveWeightingGameObjects[i]);
				}
			}

			for (int i = octaveWeightingInputs.Length; i < count; i++)
			{
				// Instantiate new gameobject
				var go = Object.Instantiate(octaveWeightingTemplate, weightingsContainer.transform);
				var pos = octaveWeightingTemplate.transform.localPosition;
				go.transform.localPosition = new Vector3(pos.x, pos.y - (40 * i), pos.z);
				go.SetActive(true);
				newGos[i] = go;
				newInputs[i] = go.GetComponentInChildren<InputField>();
				newInputs[i].text = "0";
				newInputs[i].onEndEdit.AddListener(CreateFloatValidationDelegate(newInputs[i]));
				go.GetComponentInChildren<Text>().text = "Octave " + i;
			}

			octaveWeightingGameObjects = newGos;
			octaveWeightingInputs = newInputs;
			(weightingsContainer.transform as RectTransform).sizeDelta = new Vector2(100, (count + 1) * 40);
		}

		private UnityEngine.Events.UnityAction<string> CreateFloatValidationDelegate(InputField inputField)
		{
			return (value) => {
				float result = 1.0f;
				if (!float.TryParse(value, out result) || result < 0)
				{
					inputField.text = "0";
				}
			};
		}

		private GenerationParameters GetGenerationParameters()
		{
			var weightings = new float[octaveWeightingInputs.Length];
			for (int i = 0, l = octaveWeightingInputs.Length; i < l; i++)
			{
				weightings[i] = float.Parse(octaveWeightingInputs[i].text);
			}

			return new GenerationParameters
			{
				Seed = int.Parse(seedValue.text),
				HorizontalExtents = int.Parse(horizontalExtents.text),
				VerticalExtents = int.Parse(verticalExtents.text),
				BaseWavelength = int.Parse(baseWavelength.text),
				OctaveWeightings = weightings,
			};
		}
	}
}
