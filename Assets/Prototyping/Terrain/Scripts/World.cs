﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Prototyping
{
	public class World
	{
		// Arguably calling the things called 'positions' 
		// in this case are in chunk index space, rather 
		// than world space, so it's a bit misleadingly named

		public readonly int chunkSize = 16;

		private Dictionary<string, Chunk> chunksByPosition = new Dictionary<string, Chunk>();
		private Dictionary<string, IntVector3> positions = new Dictionary<string, IntVector3>();
		private WorldBehaviour worldInstance;

		public void InstantiateWorld(GameObject worldRoot, BlockConfig config, Action callback)
		{
			worldInstance = worldRoot.AddComponent<WorldBehaviour>();
			worldInstance.Init(config, this);
			worldInstance.StartCoroutine(InstantiateWorldRoutine(callback));
		}

		IEnumerator InstantiateWorldRoutine(Action callback)
		{
			int count = 0;
			foreach (var chunk in chunksByPosition.Values)
			{
				InstantiateChunk(chunk);
				count++;
				if (count >= 16)
				{
					yield return null;
					count = 0;
				}
			}
			callback();
		}

		public int GetChunkInstanceCount()
		{
			if (worldInstance != null)
			{
				return worldInstance.GetChunkInstanceCount();
			}
			return 0;
		}

		public Chunk CreateChunk(IntVector3 position)
		{
			var chunk = new Chunk(this.chunkSize);
			positions[chunk.id] = position;
			chunksByPosition[position.ToString()] = chunk;
			return chunk;
		}

		public void InstantiateChunk(Chunk chunk)
		{
			if (positions.ContainsKey(chunk.id))
			{
				worldInstance.InstantiateChunk(chunk, chunkSize * positions[chunk.id].ToVector3());
			}
			else
			{
				Debug.LogErrorFormat("Unable to instantiate chunk {0}, has not been created via this class", chunk.id);
			}
		}

		public void UpdateChunk(Chunk chunk, params IntVector3[] updatedBlockPositions)
		{
			if (!worldInstance.ChunkInstanceExists(chunk))
			{
				this.InstantiateChunk(chunk);
			}
			else
			{
				worldInstance.UpdateChunk(chunk);
			}

			// Check to see if we need to update adacent blocks
			var updatedChunks = new HashSet<string>();
			for (int i = 0, l = updatedBlockPositions.Length; i < l; i++)
			{
				if (updatedBlockPositions[i].x == 0)
				{
					CheckAndUpdateAdjacentChunk(chunk, Vector3.left, updatedChunks);
				}
				if (updatedBlockPositions[i].x == this.chunkSize - 1)
				{
					CheckAndUpdateAdjacentChunk(chunk, Vector3.right, updatedChunks);
				}
				if (updatedBlockPositions[i].y == 0)
				{
					CheckAndUpdateAdjacentChunk(chunk, Vector3.down, updatedChunks);
				}
				if (updatedBlockPositions[i].y == this.chunkSize - 1)
				{
					CheckAndUpdateAdjacentChunk(chunk, Vector3.up, updatedChunks);
				}
				if (updatedBlockPositions[i].z == 0)
				{
					CheckAndUpdateAdjacentChunk(chunk, Vector3.back, updatedChunks);
				}
				if (updatedBlockPositions[i].z == this.chunkSize - 1)
				{
					CheckAndUpdateAdjacentChunk(chunk, Vector3.forward, updatedChunks);
				}
			}
		}

		private void CheckAndUpdateAdjacentChunk(Chunk chunk, Vector3 direction, HashSet<string> updatedChunks)
		{
			var adjacentChunk = GetAdjacentChunk(chunk, new IntVector3(direction));
			if (adjacentChunk != null && !updatedChunks.Contains(adjacentChunk.id))
			{
				updatedChunks.Add(adjacentChunk.id);
				UpdateChunk(adjacentChunk);
			}
		}

		public Chunk GetChunkByPosition(IntVector3 chunkPosition)
		{
			var positionKey = chunkPosition.ToString();
			if (chunksByPosition.ContainsKey(positionKey))
			{
				return chunksByPosition[positionKey];
			}
			return null;
		}

		public Chunk GetChunkAbove(Chunk chunk)
		{
			return GetAdjacentChunk(chunk, new IntVector3(0, 1, 0));
		}

		public Chunk GetAdjacentChunk(Chunk chunk, IntVector3 direction)
		{
			if (positions.ContainsKey(chunk.id))
			{
				var position = positions[chunk.id];
				position = new IntVector3(position.x + direction.x, position.y + direction.y, position.z + direction.z);
				var key = position.ToString();
				if (chunksByPosition.ContainsKey(key))
				{
					return chunksByPosition[key];
				}
			}
			return null;
		}

		public IntVector3 GetChunkPosition(Vector3 worldPosition)
		{
			IntVector3 blockPosition, chunkPosition;
			GetPositions(worldPosition, out chunkPosition, out blockPosition);
			return chunkPosition;
		}

		public int GetBlock(Chunk chunk, int i, int j, int k)
		{
			Chunk adjacentChunk = null;
			if (i < 0 || j < 0 || k < 0 || i >= chunkSize || j >= chunkSize || k >= chunkSize)
			{
				IntVector3 direction = new IntVector3();
				if (i < 0)
				{
					direction.x = -1;
					i = chunkSize - 1;
				}
				else if (i >= chunkSize)
				{
					direction.x = +1;
					i = 0;
				}
				if (j < 0)
				{
					direction.y = -1;
					j = chunkSize - 1;
				}
				else if (j >= chunkSize)
				{
					direction.y = +1;
					j = 0;
				}
				if (k < 0)
				{
					direction.z = -1;
					k = chunkSize - 1;
				}
				else if (k >= chunkSize)
				{
					direction.z = +1;
					k = 0;
				}
				adjacentChunk = GetAdjacentChunk(chunk, direction);
				if (adjacentChunk != null)
				{
					return adjacentChunk.GetBlock(i, j, k);
				}
				return 0;	// Treat "off the map" as empty blocks
			}
			return chunk.GetBlock(i, j, k);
		}

		public IntVector3 GetBlockPosition(Vector3 worldPosition)
		{
			IntVector3 blockPosition, chunkPosition;
			GetPositions(worldPosition, out chunkPosition, out blockPosition);
			return blockPosition;
		}

		public void GetPositions(Vector3 worldPosition, out IntVector3 chunkPosition, out IntVector3 blockPosition)
		{
			var chunkOffset = worldPosition / chunkSize;
			chunkPosition = new IntVector3(Mathf.RoundToInt(chunkOffset.x), Mathf.RoundToInt(chunkOffset.y), Mathf.RoundToInt(chunkOffset.z));

			var chunkWorldPosition = chunkSize * chunkPosition.ToVector3();
			var blockOffset = worldPosition - chunkWorldPosition;
			blockPosition = new IntVector3(blockOffset + Mathf.Floor(chunkSize / 2f) * Vector3.one);
			// Well this is a massive hack! :D
			if (blockPosition.y == 16)
			{
				blockPosition = new IntVector3(blockPosition.x, 0, blockPosition.z);
				chunkPosition = new IntVector3(chunkPosition.x, chunkPosition.y + 1, chunkPosition.z);
			}
			if (blockPosition.x == 16)
			{
				blockPosition = new IntVector3(0, blockPosition.y, blockPosition.z);
				chunkPosition = new IntVector3(chunkPosition.x + 1, chunkPosition.y, chunkPosition.z);
			}
			if (blockPosition.z == 16)
			{
				blockPosition = new IntVector3(blockPosition.x, blockPosition.y, 0);
				chunkPosition = new IntVector3(chunkPosition.x, chunkPosition.y, chunkPosition.z + 1);
			}
		}

		public int GetBlockAtPosition(Vector3 worldPosition)
		{
			IntVector3 chunkPosition, blockPosition;
			GetPositions(worldPosition, out chunkPosition, out blockPosition);
			Chunk chunk = GetChunkByPosition(chunkPosition);
			if (chunk != null)
			{
				return GetBlock(chunk, blockPosition.x, blockPosition.y, blockPosition.z);
			}
			return 0;
		}

		public Vector3 GetWorldPositionForBlock(IntVector3 chunkPosition, IntVector3 blockPosition)
		{
			return chunkSize * chunkPosition.ToVector3() + (blockPosition.ToVector3() - (chunkSize / 2f) * Vector3.one);
		}
	}
}
