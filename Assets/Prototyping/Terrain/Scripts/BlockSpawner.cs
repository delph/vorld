﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Prototyping
{
	public class BlockSpawner : MonoBehaviour
	{
		BlockConfig blockConfig;
		World world;
		int selectedBlock = 1;
		int layerMask = 0;
		GameObject blockHighlight;
		Material highlightMaterial;

		void Awake()
		{
			layerMask = 1 << LayerMask.NameToLayer("Terrain") | 1 << LayerMask.NameToLayer("Water");
			highlightMaterial = Resources.Load<Material>("UnlitBlack");
			blockHighlight = new GameObject("BlockHighLight");
			blockHighlight.SetActive(false);
			AddLineRenderersToHighlight();
		}

		private void AddLineRenderersToHighlight()
		{
			LineRenderer lineRenderer;

			lineRenderer = AddLineRendererWithPositions(new Vector3[] { 
				0.5f * Vector3.forward + 0.5f * Vector3.up + 0.5f * Vector3.left, // Top Left
				0.5f * Vector3.forward + 0.5f * Vector3.up + 0.5f * Vector3.right, // Top Right
 				0.5f * Vector3.forward + 0.5f * Vector3.down + 0.5f * Vector3.right, // Bottom Right
				0.5f * Vector3.forward + 0.5f * Vector3.down + 0.5f * Vector3.left,  // Bottom Left
				0.5f * Vector3.forward + 0.5f * Vector3.up + 0.5f * Vector3.left, // Back to top left
			});

			lineRenderer = AddLineRendererWithPositions(new Vector3[] { 
				0.5f * Vector3.back + 0.5f * Vector3.up + 0.5f * Vector3.left, // Top Left
				0.5f * Vector3.back + 0.5f * Vector3.up + 0.5f * Vector3.right, // Top Right
 				0.5f * Vector3.back + 0.5f * Vector3.down + 0.5f * Vector3.right, // Bottom Right
				0.5f * Vector3.back + 0.5f * Vector3.down + 0.5f * Vector3.left,  // Bottom Left
				0.5f * Vector3.back + 0.5f * Vector3.up + 0.5f * Vector3.left, // Back to top left
			});

			lineRenderer = AddLineRendererWithPositions(new Vector3[] { 
				0.5f * Vector3.forward + 0.5f * Vector3.up + 0.5f * Vector3.left, // Top Left
				0.5f * Vector3.back + 0.5f * Vector3.up + 0.5f * Vector3.left, 
			});

			lineRenderer = AddLineRendererWithPositions(new Vector3[] { 
				0.5f * Vector3.forward + 0.5f * Vector3.up + 0.5f * Vector3.right, // Top Right
				0.5f * Vector3.back + 0.5f * Vector3.up + 0.5f * Vector3.right, 
			});

			lineRenderer = AddLineRendererWithPositions(new Vector3[] { 
				0.5f * Vector3.forward + 0.5f * Vector3.down + 0.5f * Vector3.left, // Bottom Left
				0.5f * Vector3.back + 0.5f * Vector3.down + 0.5f * Vector3.left, 
			});

			lineRenderer = AddLineRendererWithPositions(new Vector3[] { 
				0.5f * Vector3.forward + 0.5f * Vector3.down + 0.5f * Vector3.right, // Bottom Right
				0.5f * Vector3.back + 0.5f * Vector3.down + 0.5f * Vector3.right, 
			});
		}

		private LineRenderer AddLineRendererWithPositions(Vector3[] positions)
		{
			var go = new GameObject("LineRenderer");
			LineRenderer lineRenderer;
			go.transform.parent = blockHighlight.transform;
			go.transform.localPosition = Vector3.zero;
			lineRenderer = go.AddComponent<LineRenderer>();
			lineRenderer.positionCount = positions.Length;
			lineRenderer.SetPositions(positions);
			lineRenderer.startWidth = 0.02f;
			lineRenderer.endWidth = 0.02f;
			lineRenderer.useWorldSpace = false;
			lineRenderer.material = highlightMaterial;
			return lineRenderer;
		}

		public void Init(BlockConfig blockConfig, World world)
		{
			this.blockConfig = blockConfig;
			this.world = world;
		}

		void Update()
		{
			RaycastHit hitInfo;
			if (Physics.Raycast(this.transform.position, this.transform.forward, out hitInfo, 6f, layerMask))
			{
				IntVector3 chunkPosition, blockPosition;
				world.GetPositions(hitInfo.point - 0.5f * hitInfo.normal, out chunkPosition, out blockPosition);
				Vector3 blockWorldPosition = world.GetWorldPositionForBlock(chunkPosition, blockPosition);
				blockHighlight.transform.position = blockWorldPosition;
				blockHighlight.SetActive(true);
			}
			else
			{
				blockHighlight.SetActive(false);
			}

			// Either the mesh collider raycast info or the get positions method is a bit funny
			if (Input.GetMouseButtonDown(0))
			{
				if (Physics.Raycast(this.transform.position, this.transform.forward, out hitInfo, 6f, layerMask))
				{
					IntVector3 chunkPosition, blockPosition;
					world.GetPositions(hitInfo.point - 0.5f * hitInfo.normal, out chunkPosition, out blockPosition);

					var chunk = world.GetChunkByPosition(chunkPosition);
					if (!blockConfig.blocks[chunk.GetBlock(blockPosition.x, blockPosition.y, blockPosition.z)].isIndestructible)
					{
						// TODO: pop noise please!
						chunk.RemoveBlock(blockPosition.x, blockPosition.y, blockPosition.z);
						world.UpdateChunk(chunk, blockPosition);
					}
					// TODO: else feedback
				}
			}
			if (Input.GetMouseButtonDown(1))
			{
				if (Physics.Raycast(this.transform.position, this.transform.forward, out hitInfo, 5f, layerMask))
				{
					IntVector3 chunkPosition, blockPosition;
					world.GetPositions(hitInfo.point + 0.5f * hitInfo.normal, out chunkPosition, out blockPosition);

					Vector3 blockWorldPosition = world.GetWorldPositionForBlock(chunkPosition, blockPosition);
					if (Physics.OverlapBox(blockWorldPosition, 0.4999f * Vector3.one).Length == 0)
					{
						Chunk.Orientation orientation = Chunk.Orientation.Up;
						if (hitInfo.normal == Vector3.left)
						{
							orientation = Chunk.Orientation.Left;
						}
						else if (hitInfo.normal == Vector3.right)
						{
							orientation = Chunk.Orientation.Right;
						}
						else if (hitInfo.normal == Vector3.down)
						{
							orientation = Chunk.Orientation.Down;
						}
						else if (hitInfo.normal == Vector3.forward)
						{
							orientation = Chunk.Orientation.Foward;
						}
						else if (hitInfo.normal == Vector3.back)
						{
							orientation = Chunk.Orientation.Back;
						}

						var chunk = world.GetChunkByPosition(chunkPosition);
						if (chunk == null)
						{
							chunk = world.CreateChunk(chunkPosition);
						}
						chunk.AddBlock(selectedBlock, blockPosition.x, blockPosition.y, blockPosition.z, orientation);
						world.UpdateChunk(chunk, blockPosition);
					}
				}
			}
			if (Input.mouseScrollDelta.y != 0)
			{
				int delta = (int)Mathf.Sign(Input.mouseScrollDelta.y);
				selectedBlock += delta;
				if (selectedBlock <= 0)
				{
					selectedBlock = blockConfig.blocks.Length - 1;
				}
				else if (selectedBlock >= blockConfig.blocks.Length)
				{
					selectedBlock = 1;
				}
			}
		}

		void OnGUI()
		{
			if (selectedBlock < blockConfig.blocks.Length)
			{
				GUILayout.Label("Selected Block: " + blockConfig.blocks[selectedBlock].name + " (Scroll to Change)");
			}
		}

	}
}
