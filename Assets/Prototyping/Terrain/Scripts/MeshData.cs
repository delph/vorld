﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace Prototyping
{
	public class MeshData
	{
		public List<Vector3> vertices = new List<Vector3>();
		public List<Vector3> normals = new List<Vector3>();
		public List<Vector2> uvs = new List<Vector2>();
		public List<int> triangles = new List<int>();

		public MeshData Scale(float amount)
		{
			for(int i = 0, l = vertices.Count; i < l; i++)
			{
				vertices[i] *= amount;
			}
			return this;
		}

		public MeshData Offset(Vector3 offset)
		{
			for(int i = 0, l = vertices.Count; i < l; i++)
			{
				vertices[i] += offset;
			}
			return this;
		}

		public void Combine(MeshData incoming, Vector3? offset = null, float? scale = null)
		{
			int vertexCount = this.vertices.Count;
			int triangleCount = this.triangles.Count;
			this.vertices.AddRange(incoming.vertices);
			this.normals.AddRange(incoming.normals);
			this.uvs.AddRange(incoming.uvs);
			this.triangles.AddRange(incoming.triangles);
			if(offset.HasValue)
			{
				for(int i = vertexCount, l = this.vertices.Count; i < l; i++)
				{
					if(scale.HasValue)
					{
						this.vertices[i] = scale.Value * this.vertices[i] + offset.Value;
					}
					else
					{
						this.vertices[i] += offset.Value;
					}
				}
			}
			for (int i = triangleCount, l = this.triangles.Count; i < l; i++)
			{
				this.triangles[i] += vertexCount;
				if (triangles[i] >= this.vertices.Count)
				{
					throw new System.IndexOutOfRangeException(string.Format(
						"Tried to set index of {0} ({3} + {2}) when there are {1} vertices", triangles[i], this.vertices.Count, vertexCount, triangles[i] - vertexCount));
				}
			}
		}

		public void AdjustQuadTextureCoordinates(int uvIndexOffset, int atlasTileSize, int atlasWidth, int atlasHeight, float[] pixelOffset, int cycleUvs = 0)
		{
			// Need to be cycled to rotate the texture
			for(int i = uvIndexOffset; i < uvIndexOffset + 4; i++)
			{
				this.uvs[i] = new Vector2((this.uvs[i].x * atlasTileSize + pixelOffset[0]) / (float)atlasWidth, (this.uvs[i].y * atlasTileSize  + pixelOffset[1]) / (float)atlasHeight);
			}

			cycleUvs = cycleUvs % 4;
			if (cycleUvs != 0)
			{
				Vector2 t;
				if (cycleUvs == 1)
				{
					t = this.uvs[uvIndexOffset];
					this.uvs[uvIndexOffset] = this.uvs[uvIndexOffset + 3];
					this.uvs[uvIndexOffset + 3] = this.uvs[uvIndexOffset + 2];
					this.uvs[uvIndexOffset + 2] = this.uvs[uvIndexOffset + 1];
					this.uvs[uvIndexOffset + 1] = t;
				}
				if (cycleUvs == 2)
				{
					t = this.uvs[uvIndexOffset];
					this.uvs[uvIndexOffset] = this.uvs[uvIndexOffset + 2];
					this.uvs[uvIndexOffset + 2] = t;
					t = this.uvs[uvIndexOffset + 1];
					this.uvs[uvIndexOffset + 1] = this.uvs[uvIndexOffset + 3];
					this.uvs[uvIndexOffset + 3] = t;
				}
				if (cycleUvs == 3)
				{
					t = this.uvs[uvIndexOffset];
					this.uvs[uvIndexOffset] = this.uvs[uvIndexOffset + 1];
					this.uvs[uvIndexOffset + 1] = this.uvs[uvIndexOffset + 2];
					this.uvs[uvIndexOffset + 2] = this.uvs[uvIndexOffset + 3];
					this.uvs[uvIndexOffset + 3] = t;
				}
			}
		}

		public Mesh ToMesh()
		{
			var mesh = new Mesh();
			mesh.vertices = vertices.ToArray();
			mesh.normals = normals.ToArray();
			mesh.uv = uvs.ToArray();
			mesh.triangles = triangles.ToArray();
			return mesh;
		}
	}

	public static class CubeMeshData
	{
		public static MeshData Front = new MeshData {
			vertices = new List<Vector3> { new Vector3(-1.0f, -1.0f, 1.0f), new Vector3(1.0f, -1.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f), new Vector3(-1.0f, 1.0f, 1.0f) },
			normals = new List<Vector3> { new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 0.0f, 1.0f) },
			uvs = new List<Vector2> { new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f), new Vector2(1.0f, 1.0f), new Vector2(0.0f, 1.0f) },
			triangles = new List<int> { 0, 1, 2, 0, 2, 3 }
		};

		public static MeshData Back = new MeshData {
			vertices = new List<Vector3> { new Vector3(-1.0f, -1.0f, -1.0f), new Vector3(-1.0f, 1.0f, -1.0f), new Vector3(1.0f, 1.0f, -1.0f), new Vector3(1.0f, -1.0f, -1.0f) },
			normals = new List<Vector3> { new Vector3(0.0f, 0.0f, -1.0f), new Vector3(0.0f, 0.0f, -1.0f), new Vector3(0.0f, 0.0f, -1.0f), new Vector3(0.0f, 0.0f, -1.0f) },
			uvs = new List<Vector2> { new Vector2(1.0f, 0.0f), new Vector2(1.0f, 1.0f), new Vector2(0.0f, 1.0f), new Vector2(0.0f, 0.0f) },
			triangles = new List<int> { 0, 1, 2, 0, 2, 3 }
		};

		public static MeshData Top = new MeshData {
			vertices = new List<Vector3> { new Vector3(-1.0f, 1.0f, -1.0f), new Vector3(-1.0f, 1.0f, 1.0f), new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, 1.0f, -1.0f) },
			normals = new List<Vector3> { new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f) },
			uvs = new List<Vector2> { new Vector2(0.0f, 1.0f), new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f), new Vector2(1.0f, 1.0f) },
			triangles = new List<int> { 0, 1, 2, 0, 2, 3 }
		};

		public static MeshData Bottom = new MeshData {
			vertices = new List<Vector3> {  new Vector3(-1.0f, -1.0f, -1.0f), new Vector3(1.0f, -1.0f, -1.0f), new Vector3(1.0f, -1.0f, 1.0f), new Vector3(-1.0f, -1.0f, 1.0f) },
			normals = new List<Vector3> { new Vector3(0.0f, -1.0f, 0.0f), new Vector3(0.0f, -1.0f, 0.0f), new Vector3(0.0f, -1.0f, 0.0f), new Vector3(0.0f, -1.0f, 0.0f) },
			uvs = new List<Vector2> { new Vector2(1.0f, 1.0f), new Vector2(0.0f, 1.0f), new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f) },
			triangles = new List<int> { 0, 1, 2, 0, 2, 3 }
		};

		public static MeshData Right = new MeshData {
			vertices = new List<Vector3> { new Vector3(1.0f, -1.0f, -1.0f), new Vector3(1.0f, 1.0f, -1.0f), new Vector3(1.0f, 1.0f, 1.0f), new Vector3(1.0f, -1.0f, 1.0f) },
			normals = new List<Vector3> { new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f) },
			uvs = new List<Vector2> { new Vector2(1.0f, 0.0f), new Vector2(1.0f, 1.0f), new Vector2(0.0f, 1.0f), new Vector2(0.0f, 0.0f) },
			triangles = new List<int> { 0, 1, 2, 0, 2, 3 }
		};

		public static MeshData Left = new MeshData {
			vertices = new List<Vector3> {  new Vector3(-1.0f, -1.0f, -1.0f), new Vector3(-1.0f, -1.0f, 1.0f), new Vector3(-1.0f, 1.0f, 1.0f), new Vector3(-1.0f, 1.0f, -1.0f) },
			normals = new List<Vector3> { new Vector3(-1.0f, 0.0f, 0.0f), new Vector3(-1.0f, 0.0f, 0.0f), new Vector3(-1.0f, 0.0f, 0.0f), new Vector3(-1.0f, 0.0f, 0.0f) },
			uvs = new List<Vector2> { new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f), new Vector2(1.0f, 1.0f), new Vector2(0.0f, 1.0f) },
			triangles = new List<int> { 0, 1, 2, 0, 2, 3 }
		};

		public static MeshData Invert(MeshData meshData)
		{
			var normals = new List<Vector3>(meshData.normals);
			for(int i = 0, l = normals.Count; i < l; i++)
			{
				normals[i] = -1f * normals[i];
			}

			var triangles = new List<int>(meshData.triangles);
			for (int i = 0, l = triangles.Count / 3; i < l; i++)
			{
				int secondTriangleIndex = triangles[(3 * i) + 1];
				int thirdTriangleIndex = triangles[(3 * i) + 2];

				triangles[(3 * i) + 1] = thirdTriangleIndex;
				triangles[(3 * i) + 2] = secondTriangleIndex;
			}

			var result = new MeshData();
			result.vertices = new List<Vector3>(meshData.vertices);
			result.normals = normals;
			result.uvs = new List<Vector2>(meshData.uvs);
			result.triangles = triangles;
			return result;
		}
	}
}
