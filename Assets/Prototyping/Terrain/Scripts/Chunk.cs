﻿using System.Collections.Generic;
using UnityEngine;
namespace Prototyping
{
	public class Chunk
	{
		public enum Orientation
		{
			Up,
			Left,
			Foward,
			Right,
			Back,
			Down,
		}

		public string id { get; private set; }
		public int size;
		int[] blocks;
		Orientation[] orientations; // up direction for each block

		// Ambient Light map for  Chunk can be 64 x 64 map

		public Chunk(int size)
		{
			this.id = System.Guid.NewGuid().ToString();
			this.size = size;
			this.blocks = new int[size + (size - 1)  * size + (size - 1) * size * size];
			this.orientations = new Orientation[size + (size - 1) * size + (size - 1) * size * size];
		}
		
		public int CalculateChunkIndex(int i, int j, int k)
		{
			return i + j * this.size + k * this.size * this.size;
		}

		public void AddBlock(int value, int i, int j, int k, Orientation orientation = Orientation.Up)
		{
			int index = CalculateChunkIndex(i, j, k);
			this.blocks[index] = value;
			this.orientations[index] = orientation;
		}

		public void RemoveBlock(int i, int j, int k)
		{
			int index = CalculateChunkIndex(i, j, k);
			this.blocks[index] = 0;
			this.orientations[index] = 0;
		}

		public int GetBlock(int i, int j, int k)
		{
			if (i < 0 || j < 0 || k < 0 || i >= this.size || j >= this.size || k >= this.size)
			{
				return -1;
			}
			return this.blocks[CalculateChunkIndex(i, j, k)];
		}

		public Orientation GetBlockOrientation(int i, int j, int k)
		{
			if (i < 0 || j < 0 || k < 0 || i >= this.size || j >= this.size || k >= this.size)
			{
				return 0; 
			}
			return this.orientations[CalculateChunkIndex(i, j, k)];
		}

		public HashSet<int> GetBlockIds()
		{
			var ids = new HashSet<int>();
			for(int i = 0, l = blocks.Length; i < l; i++)
			{
				if(!ids.Contains(blocks[i]))
				{
					ids.Add(blocks[i]);
				}
			}
			return ids;
		}
	}
}