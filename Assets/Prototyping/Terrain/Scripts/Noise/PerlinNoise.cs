﻿using UnityEngine;

// Converted from https://gist.github.com/banksean/304522
//
// Ported from Stefan Gustavson's java implementation
// http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
// Read Stefan's excellent paper for details on how this code works.

public class PerlinNoise
{
	private int[][] grad3 = new int[][] {
		new int[] { 1,1,0 }, new int[] { -1,1,0 }, new int[] { 1,-1,0 }, new int[] { -1,-1,0 },
		new int[] { 1,0,1 }, new int[] { -1,0,1 }, new int[] { 1,0,-1 }, new int[] { -1,0,-1 },
		new int[] { 0,1,1 }, new int[] { 0,-1,1 }, new int[] { 0,1,-1 }, new int[] { 0,-1,-1 } };

	private int[] p = new int[256];
	private int[] perm = new int[512];

	public PerlinNoise(int seed)
	{
		Random.seed = seed;
		for (int i = 0; i < 256; i++)
		{
			this.p[i] = Mathf.FloorToInt(Random.value * 256);
		}

		// To remove the need for index wrapping, double the permutation table length
		for (int i = 0; i < 512; i++)
		{
			this.perm[i] = this.p[i & 255];
		}
	}

	public float Noise(float x, float y, float z) {
		// Find unit grid cell containing point
		var X = Mathf.FloorToInt(x);
		var Y = Mathf.FloorToInt(y);
		var Z = Mathf.FloorToInt(z);

		// Get relative xyz coordinates of point within that cell
		x = x - X;
		y = y - Y;
		z = z - Z;

		// Wrap the integer cells at 255 (smaller integer period can be introduced here)
		X = X & 255;
		Y = Y & 255;
		Z = Z & 255;

		// Calculate a set of eight hashed gradient indices
		var gi000 = this.perm[X+this.perm[Y+this.perm[Z]]] % 12;
		var gi001 = this.perm[X+this.perm[Y+this.perm[Z+1]]] % 12;
		var gi010 = this.perm[X+this.perm[Y+1+this.perm[Z]]] % 12;
		var gi011 = this.perm[X+this.perm[Y+1+this.perm[Z+1]]] % 12;
		var gi100 = this.perm[X+1+this.perm[Y+this.perm[Z]]] % 12;
		var gi101 = this.perm[X+1+this.perm[Y+this.perm[Z+1]]] % 12;
		var gi110 = this.perm[X+1+this.perm[Y+1+this.perm[Z]]] % 12;
		var gi111 = this.perm[X+1+this.perm[Y+1+this.perm[Z+1]]] % 12;

		// The gradients of each corner are now:
		// g000 = grad3[gi000];
		// g001 = grad3[gi001];
		// g010 = grad3[gi010];
		// g011 = grad3[gi011];
		// g100 = grad3[gi100];
		// g101 = grad3[gi101];
		// g110 = grad3[gi110];
		// g111 = grad3[gi111];
		// Calculate noise contributions from each of the eight corners
		var n000= this.Dot(this.grad3[gi000], x, y, z);
		var n100= this.Dot(this.grad3[gi100], x-1, y, z);
		var n010= this.Dot(this.grad3[gi010], x, y-1, z);
		var n110= this.Dot(this.grad3[gi110], x-1, y-1, z);
		var n001= this.Dot(this.grad3[gi001], x, y, z-1);
		var n101= this.Dot(this.grad3[gi101], x-1, y, z-1);
		var n011= this.Dot(this.grad3[gi011], x, y-1, z-1);
		var n111= this.Dot(this.grad3[gi111], x-1, y-1, z-1);
		// Compute the fade curve value for each of x, y, z
		var u = this.Fade(x);
		var v = this.Fade(y);
		var w = this.Fade(z);
		 // Interpolate along x the contributions from each of the corners
		var nx00 = this.Mix(n000, n100, u);
		var nx01 = this.Mix(n001, n101, u);
		var nx10 = this.Mix(n010, n110, u);
		var nx11 = this.Mix(n011, n111, u);
		// Interpolate the four results along y
		var nxy0 = this.Mix(nx00, nx10, v);
		var nxy1 = this.Mix(nx01, nx11, v);
		// Interpolate the two last results along z
		var nxyz = this.Mix(nxy0, nxy1, w);

		return nxyz;
	}

	float Dot(int[] g, float x, float y, float z)
	{
		return g[0] * x + g[1] * y + g[2] * z;
	}

	float Mix(float a, float b, float t)
	{
		return (1.0f - t) * a + t * b;
	}

	float Fade(float t)
	{
		return t * t * t * (t * (t * 6.0f - 15.0f) + 10.0f);
	}
}
