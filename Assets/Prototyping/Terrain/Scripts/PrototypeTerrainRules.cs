﻿using UnityEngine;
using System.Collections;

namespace Prototyping
{
	public class PrototypeTerrainRules : TerrainRules, ITerrainWithWater
	{
		public enum BaseFunctionType
		{
			None,
			NegativeY,
			InverseY,
			Gaussian,
		}

		[SerializeField]
		int bedRockId;
		[SerializeField]
		int waterId;
		[SerializeField]
		BaseFunctionType baseFunction;
		[SerializeField]
		float[] baseFunctionParameters;
		
		public bool IsBlockIdWater(int blockId)
		{
			return blockId == waterId;
		}

		public override float BaseFunction(float x, float y, float z)
		{
			float adjust = 1f;
			switch (this.baseFunction)
			{
				case BaseFunctionType.NegativeY:
					// adjust = (-y-intercept)/range? Where intercept is the point at which it'll be 0 and range is the length back from intercept to reach 1
					adjust = (-y + 48f) / 16f;
					break;
				case BaseFunctionType.InverseY:
					float adjustementFactor = baseFunctionParameters[0], maxDepth = baseFunctionParameters[1];
					// 0.01 and 16 are good factors for adjustmentFactor and maxDepth accordingly
					adjust = 1 / (adjustementFactor * (maxDepth + y));
					break;
				case BaseFunctionType.Gaussian:
					// Gaussian
					// f(x,z) = A * exp(- ( (x-x0)^2 / 2*sdx^2  +  (z-z0)^2 / 2*sdz^2 ) )
					float a = baseFunctionParameters[0], x0 = 0f, z0 = 0f, sdx = baseFunctionParameters[1], sdz = baseFunctionParameters[1];
					// 48 is a goodish a, 96 is a good sd
					float fxy = a * Mathf.Exp(-((((x - x0) * (x - x0)) / (2 * sdx * sdx)) + (((z - z0) * (z - z0)) / (2 * sdz * sdz))));
					adjust = 1 + (fxy - y) / 16f;
					break;
			}
			return adjust;
		}

		public override int MapNoiseToBlock(float noiseValue)
		{
			if (noiseValue < 0.5)
			{
				return 0;
			}
			if (noiseValue < 0.8)
			{
				return 1;
			}
			return 3;
		}

		public override int TransformBlock(
			World worldInfo,
			Chunk currentChunk,
			float worldY,
			int x,
			int y,
			int z,
			int block)
		{
			if (worldY >= 0
				&& block == 1 // Is Soil
				&& worldInfo.GetBlock(currentChunk, x, y + 1, z) == 0)
			{
				return 2; // Make it grass!
			}
			else if (worldY < 0 && y == 0)
			{
				return bedRockId;
			}
			else if (worldY < 0 && block == 0)
			{
				// Can't really include water until greedy meshing taken care of
				// return waterId;
			}
			return block;
		}
	}
}

