﻿using System.Collections.Generic;
using UnityEngine;

namespace Prototyping
{
	[System.Serializable]
	public class Block
	{
		public int id;
		public string name;
		public bool isIndestructible;
		public bool isTransparent;
		public bool isLiquid;
		public bool useCutout;
		public AtlasPosition atlasIndices;
		public Color color = Color.white;
	}
}