﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototyping
{
	public class TerrainExperimentController : MonoBehaviour
	{
		[SerializeField]
		GameObject _uiPanel;
		[SerializeField]
		UnityEngine.UI.Text _panelText;
		[SerializeField]
		VoxelTerrain _voxelTerrain;
		[SerializeField]
		UI.GenerationPanel _generationPanel;

		void Awake()
		{
			if (_voxelTerrain == null)
			{
				Debug.LogWarning("Voxel Terrain field unassigned, disabling TerrainExperimentController");
				this.gameObject.SetActive(false);
			}
			else if (!_voxelTerrain.generateOnAwake)
			{
				if (_generationPanel != null)
				{
					_generationPanel.Init(_voxelTerrain);
					_generationPanel.gameObject.SetActive(true);
				}
			}
			else if (_generationPanel != null)
			{
				_generationPanel.gameObject.SetActive(false);
			}
		}

		void Update()
		{
			int chunksGenerated = _voxelTerrain.ChunksGenerated;
			int chunksToGenerate = _voxelTerrain.ChunksToGenerate;
			int chunksInstantiated = _voxelTerrain.ChunksInstantiated;

			if (chunksGenerated != chunksToGenerate)
			{
				SetPanelText(string.Format("Generating Chunks {0}/{1}", chunksGenerated, chunksToGenerate));
			}
			else if (chunksInstantiated != chunksToGenerate)
			{
				SetPanelText(string.Format("Instantiating Chunks {0}/{1}", chunksInstantiated, chunksToGenerate));
			}
			else if (_uiPanel.activeInHierarchy == true)
			{
				ClosePanel();
			}

#if !UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				Application.Quit();
			}
#endif
		}

		void SetPanelText(string text)
		{
			_uiPanel.SetActive(true);
			_panelText.text = text;
		}

		void ClosePanel()
		{
			_uiPanel.SetActive(false);
		}
	}
}

