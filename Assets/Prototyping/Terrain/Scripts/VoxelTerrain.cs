﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace Prototyping
{
	public struct GenerationParameters
	{
		public int Seed;
		public float[] OctaveWeightings;
		public float BaseWavelength;
		public int HorizontalExtents;
		public int VerticalExtents;
	}

	public class VoxelTerrain : MonoBehaviour
	{
		public BlockConfig blockConfig;

		[SerializeField]
		int seed;
		[SerializeField]
		float[] octaveWeightings;
		[SerializeField]
		float baseWavelength;
		[SerializeField]
		int horizontalExtents = 32;
		[SerializeField]
		int verticalExtents = 4;
		[SerializeField]
		public bool generateOnAwake;

		public GameObject fpsControllerPrefab;
		World world;

		int chunksGenerated;
		int chunksToGenerate;

		public int ChunksGenerated { get { return chunksGenerated; } }
		public int ChunksToGenerate { get { return chunksToGenerate; } }
		public int ChunksInstantiated { get { return world == null ? 0 : world.GetChunkInstanceCount(); } }
		
		void Awake()
		{
			if (generateOnAwake)
			{
				this.StartCoroutine(GenerationRoutine());
			}
		}

		public GenerationParameters GetCurrentGenerationParameters()
		{
			return new GenerationParameters { 
				Seed = seed,
				OctaveWeightings = octaveWeightings,
				BaseWavelength = baseWavelength,
				HorizontalExtents = horizontalExtents,
				VerticalExtents = verticalExtents,
			};
		}

		public void StartGenerationRoutine(GenerationParameters generationParameters)
		{
			seed = generationParameters.Seed;
			octaveWeightings = generationParameters.OctaveWeightings;
			baseWavelength = generationParameters.BaseWavelength;
			horizontalExtents = generationParameters.HorizontalExtents;
			verticalExtents = generationParameters.VerticalExtents;

			this.StartCoroutine(GenerationRoutine());
		}

		IEnumerator GenerationRoutine()
		{
			float startTime = Time.realtimeSinceStartup;
			PerlinNoise[] octaves = new PerlinNoise[octaveWeightings.Length];
			for (int i = 0, l = octaves.Length; i < l; i++)
			{
				octaves[i] = new PerlinNoise(this.seed + i);
			}

			chunksGenerated = 0;
			chunksToGenerate = horizontalExtents * horizontalExtents * verticalExtents;

			world = new World();

			float lastYieldTime = Time.realtimeSinceStartup;
			float maximumTimeBetweenYields = 1 / 10f;
			int hMin = - horizontalExtents / 2;
			int hMax = horizontalExtents + hMin;
			for (int i = hMin; i < hMax; i++)
			{
				for (int j = verticalExtents - 1; j >= 0; j--)
				{
					for (int k = hMin; k < hMax; k++)
					{
						GenerateChunk(world, new IntVector3(i, j, k), octaves, blockConfig);
						if (Time.realtimeSinceStartup > lastYieldTime + maximumTimeBetweenYields)
						{
							yield return null;
							lastYieldTime = Time.realtimeSinceStartup;
						}
						chunksGenerated++;
					}
				}
			}

			Assert.IsTrue(chunksGenerated == chunksToGenerate);

			Debug.LogFormat("Finished generating Chunks in {0} seconds!", Time.realtimeSinceStartup - startTime);
	
			yield return null;

			startTime = Time.realtimeSinceStartup;
			world.InstantiateWorld(this.gameObject, blockConfig, () => {
				Debug.LogFormat("Finished Instantiating Chunks in {0} seconds!", Time.realtimeSinceStartup - startTime);

				var go = Object.Instantiate(fpsControllerPrefab, Vector3.up * 64f, Quaternion.identity) as GameObject;
				var cam = go.GetComponentInChildren<Camera>();
				cam.gameObject.AddComponent<BlockSpawner>().Init(blockConfig, world);
				Camera.main.gameObject.SetActive(false);
			});
		}

		Chunk GenerateChunk(World worldData, IntVector3 worldPosition, PerlinNoise[] octaves, BlockConfig config)
		{
			// Generate Voxels
			bool chunkContainsBlock = false;
			var chunk = worldData.CreateChunk(worldPosition); 

			var chunkOffset = chunk.size * worldPosition.ToVector3();
			for (int i = 0; i < chunk.size; i++)
			{
				var x = (i - Mathf.Floor(chunk.size / 2f)) + chunkOffset.x;
				for (int j = chunk.size - 1; j >= 0; j--)
				{
					var y = (j - Mathf.Floor(chunk.size / 2f)) + chunkOffset.y;
					for (int k = 0; k < chunk.size; k++)
					{
						var z = (k - Mathf.Floor(chunk.size / 2f)) + chunkOffset.z;

						float adjust = config.terrianRules.BaseFunction(x, y, z);

						float value = 0f, totalWeight = 0f;

						for (int o = 0, l = octaves.Length; o < l; o++)
						{
							var waveLength = Mathf.Pow(2, o);
							totalWeight += octaveWeightings[o];
							value += octaveWeightings[o] * octaves[o].Noise(waveLength * x / baseWavelength, waveLength * y / baseWavelength, waveLength * z / baseWavelength);
						}
						// Convert from (-1, 1) to (0, 1)
						value += 1;
						value /= 2;

						if (totalWeight > 0)
						{
							value /= totalWeight;
						}

						// ^^ Everything up to here, could arguably be performed by a Compute Shader

						int blockType = config.terrianRules.MapNoiseToBlock(value * adjust);
						// This transformation delegate is dependent on the order of world generation
						// whilst because we're being clever here we can know the data needed is generated
						// already, but it doesn't hold generically, so arguably this should be another pass
						// post full world generation (would also remove the hacky world position property in chunk)
						blockType = config.terrianRules.TransformBlock(worldData, chunk, y, i, j, k, blockType);
						if (!chunkContainsBlock && blockType != 0)
						{
							chunkContainsBlock = true;
						}
						chunk.AddBlock(blockType, i, j, k);
					}
				}
			}

			if(!chunkContainsBlock)
			{
				// No mesh required!
				return chunk;
			}

			return chunk;
		}

		private T[] Slice<T>(T[] array, int offset, int length)
		{
			T[] result = new T[length];
			System.Array.Copy(array, offset, result, 0, length);
			return result;
		}

		// TODO: Should move this to TerrainRules completely then prototype can have the worldPosition.y < 0, and cavern can have sod all
		public bool IsPositionInWater(Vector3 worldPosition)
		{
			if (worldPosition.y < 0f)
			{
				return true;
			}
			if (blockConfig.terrianRules as ITerrainWithWater != null)
			{
				return (blockConfig.terrianRules as ITerrainWithWater).IsBlockIdWater(world.GetBlockAtPosition(worldPosition));
			}
			return false;
		}
	}
}
