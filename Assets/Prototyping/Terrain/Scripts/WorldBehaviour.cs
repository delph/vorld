﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Prototyping
{
	public class WorldBehaviour : MonoBehaviour
	{
		BlockConfig config;
		World world;
		Dictionary<Chunk, GameObject> chunkInstances = new Dictionary<Chunk, GameObject>();
		Shader transparentShader;
		Shader cutoutShader;

		public void Init(BlockConfig config, World world)
		{
			this.config = config;
			this.world = world;
			transparentShader = Shader.Find("Legacy Shaders/Transparent/Diffuse");
			cutoutShader = Shader.Find("Legacy Shaders/Transparent/Cutout/Diffuse");
		}

		public int GetChunkInstanceCount()
		{
			return chunkInstances.Count;
		}

		public bool ChunkInstanceExists(Chunk chunk)
		{
			return chunkInstances.ContainsKey(chunk);
		}

		public GameObject GetInstance(Chunk chunk)
		{
			if (ChunkInstanceExists(chunk))
			{
				return chunkInstances[chunk].gameObject;
			}
			return null;
		}

		public void InstantiateChunk(Chunk chunk, Vector3 chunkOffset)
		{
			var transparentBlockMeshes = new List<TransparentBlock>();

			Mesh mesh = Meshing.CreateCulledMeshData(world, chunk, config, transparentBlockMeshes).ToMesh();
			var go = new GameObject("Chunk " + chunkOffset.ToString());
			go.layer = LayerMask.NameToLayer("Terrain");
			go.transform.parent = this.transform;
			go.transform.localPosition = chunkOffset;
			var meshFilter = go.AddComponent<MeshFilter>();
			var meshRenderer = go.AddComponent<MeshRenderer>();

			meshFilter.mesh = mesh;
			meshRenderer.material = config.atlas;
			var meshCollider = go.AddComponent<MeshCollider>();
			meshCollider.sharedMesh = mesh;
			
			chunkInstances[chunk] = go;

			foreach (var transparentBlock in transparentBlockMeshes)
			{
				SpawnTransparentBlock(go.transform, transparentBlock);
			}
		}

		public void UpdateChunk(Chunk chunk)
		{
			GameObject go = GetInstance(chunk); 
			var transparentBlocks = new List<TransparentBlock>();
			var mesh = Meshing.CreateCulledMeshData(world, chunk, config, transparentBlocks).ToMesh();
			go.GetComponent<MeshFilter>().mesh = mesh;
			go.GetComponent<MeshCollider>().sharedMesh = mesh;
			ClearAndCreateTransparentBlocks(chunkInstances[chunk].transform, transparentBlocks);
		}

		private void ClearAndCreateTransparentBlocks(Transform parent, List<TransparentBlock> transparentBlocks)
		{
			for (int i = 0, l = parent.childCount; i < l; i++)
			{
				Object.Destroy(parent.GetChild(i).gameObject);
			}

			foreach(var block in transparentBlocks)
			{
				SpawnTransparentBlock(parent, block);
			}
		}

		private void SpawnTransparentBlock(Transform parent, TransparentBlock transparentBlock)
		{
			var mesh = transparentBlock.meshData.ToMesh();
			var block = this.config.blocks[transparentBlock.blockIndex];

			var go = new GameObject("Transparent Block");
			go.layer = LayerMask.NameToLayer(block.isLiquid ? "Water" : "Terrain");
			go.transform.parent = parent;
			go.transform.localPosition = Vector3.zero;
			var meshFilter = go.AddComponent<MeshFilter>();
			var meshRenderer = go.AddComponent<MeshRenderer>();

			meshFilter.mesh = mesh;
			meshRenderer.material = config.atlas;
			meshRenderer.material.shader = block.useCutout ? this.cutoutShader : this.transparentShader;
			var meshCollider = go.AddComponent<MeshCollider>();
			meshCollider.sharedMesh = mesh;
		}
	}
}

