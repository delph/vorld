﻿using UnityEngine;
using System.Collections;

namespace Prototyping
{
	public abstract class TerrainRules : MonoBehaviour
	{
		public abstract float BaseFunction(float x, float y, float z);
		public abstract int MapNoiseToBlock(float noiseValue);
		public abstract int TransformBlock(World worldInfo, Chunk currentChunk, float worldY, int x, int y, int z, int block);
	}
}

