﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITerrainWithWater 
{
	bool IsBlockIdWater(int blockId);
}
