﻿using UnityEngine;
using System.Collections;

namespace Prototyping
{
	public class BlockConfig : MonoBehaviour
	{
		public TerrainRules terrianRules;
		public Block[] blocks;
		public Material atlas;
		public AtlasInfo atlasInfo;
	}
}

