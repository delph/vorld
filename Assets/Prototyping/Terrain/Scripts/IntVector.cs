﻿using UnityEngine;

[System.Serializable]
public struct IntVector3
{
	public IntVector3(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public IntVector3(Vector3 v3)
	{
		this.x = Mathf.RoundToInt(v3.x);
		this.y = Mathf.RoundToInt(v3.y);
		this.z = Mathf.RoundToInt(v3.z);
	}

	public int x;
	public int y;
	public int z;

	public Vector3 ToVector3()
	{
		return new Vector3(x, y, z);
	}

	public float[] ToFloatArray()
	{
		return new float[] { x, y, z };
	}

	public override string ToString()
	{
		return string.Format("{0}_{1}_{2}", x, y, z);
	}
}

[System.Serializable]
public struct IntVector2
{
	public IntVector2(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public int x;
	public int y;

	public Vector2 ToVector2()
	{
		return new Vector2(x, y);
	}

	public float[] ToFloatArray()
	{
		return new float[] { x, y };
	}

	public override string ToString()
	{
		return string.Format("{0}_{1}", x, y);
	}
}
